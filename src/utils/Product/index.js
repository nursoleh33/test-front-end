import axios from "axios";

export const getProductDetail = async ({ id }) => {
  return await axios.get(`https://dummyjson.com/products/${id}`).then((res) => {
    return res.data;
  });
};

export const getProduct = async ({ value }) => {
  return await axios
    .get(`https://dummyjson.com/products/search?q=${value}`)
    .then((res) => {
      return res.data;
    });
};
