import { getAllProduct } from "../utils/Home";
import { getProduct, getProductDetail } from "../utils/Product";
export { getAllProduct, getProduct, getProductDetail };
