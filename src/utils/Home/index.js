import axios from "axios";

export const getAllProduct = async () => {
  return await axios.get(`https://dummyjson.com/products`).then((res) => {
    return res.data;
  });
};
