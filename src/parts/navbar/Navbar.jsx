import React from "react";
import { Link, useLocation } from "react-router-dom";

const Navbar = () => {
  const { pathname } = useLocation();
  console.log("path", pathname);

  return (
    <div>
      <nav className="navbar navbar-expand-lg bg-bg-primary shadow fixed-top navbar-dark text-bg-primary text-uppercase">
        <div className="container">
          <Link className="navbar-brand" to="/">
            Logo
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item ">
                {/* JIka Navbar Aktif Maka Nav Home menjadi bold */}
                <Link
                  className={`nav-link active ${
                    pathname == "/" ? "fw-bold" : ""
                  }`}
                  aria-current="page"
                  to="/"
                >
                  Home
                </Link>
              </li>
              <li className="nav-item ">
                {/* JIka Navbar Aktif Maka Nav Home menjadi bold */}
                <Link
                  className={`nav-link active ${
                    pathname == "/product" ? "fw-bold" : ""
                  }`}
                  aria-current="page"
                  to="/product"
                >
                  Product
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
