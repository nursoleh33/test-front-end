import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { getAllProduct, getProduct, getProductDetail } from "../../utils";

const Product = () => {
  const [query, setQuery] = useState("");
  const [keyword, setKeyword] = useState("");
  const [data, setData] = useState([]);
  const nav = useNavigate();
  const handleEdit = (id) => {
    console.log("id", id);
    nav(`/detail-product/${id}`);
  };
  const handleSearch = (e) => {
    e.preventDefault();
    setKeyword(query);
    console.log("query", query);
  };
  console.log("key", keyword);
  const getDataAll = async () => {
    await getProduct({ value: keyword })
      .then((res) => {
        console.log("res", res);
        setData(res.products);
      })
      .catch((err) => {
        console.log(err);
        alert(err);
      });
  };
  useEffect(() => {
    getDataAll();
  }, [keyword]);
  console.log("data product", data);
  console.log("keyword", keyword);
  return (
    <div className="mt-5 pt-3 text-black text-capitalizex px-4">
      <div className="row">
        <div className="col-lg-12 col-sm-12 col-md-12">
          <form className="row my-3 d-flex" onSubmit={handleSearch}>
            <div className="col-auto ">
              <input
                type="text"
                className="form-control"
                placeholder="Mencari kata kunci"
                onChange={(e) => setQuery(e.target.value)}
              />
            </div>
            <div className="col-auto ">
              <button type="submit" className="btn btn-primary">
                Search
              </button>
            </div>
          </form>
        </div>
      </div>
      <div className="row">
        {data.map((item) => (
          <div className="col-sm-12 col-md-6 col-lg-4 text-center pb-3">
            <div className="card" key={item.id}>
              <img
                src={item.thumbnail}
                className="card-img-top"
                // width="50px"
                height="200px"
                alt="Test"
              />
              <div className="card-body h-auto">
                <h5 className="card-title">{item.title}</h5>
                <p className="card-text">{item.description}</p>
                <button
                  type="submit"
                  className="btn btn-primary"
                  onClick={() => handleEdit(item.id)}
                >
                  Go Detils
                </button>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Product;
