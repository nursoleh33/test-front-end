import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getAllProduct, getProduct, getProductDetail } from "../../utils";

const DetailProduct = () => {
  const [image, setImage] = useState("");
  const [data, setData] = useState({
    title: "",
    description: "",
    price: "",
    discountPercentage: "",
    rating: "",
    stock: "",
    brand: "",
    category: "",
    thumbnail: "",
    images: [],
  });
  let { id } = useParams();
  const getDataId = async (id) => {
    await getProductDetail({ id: id })
      .then((res) => {
        console.log("res", res);
        setData({
          title: res.title,
          description: res.description,
          price: res.price,
          discountPercentage: res.discountPercentage,
          rating: res.rating,
          stock: res.stock,
          brand: res.brand,
          category: res.category,
          thumbnail: res.thumbnail,
          images: res.images,
        });
        setImage(res.images[3]);
        window.scrollTo(0, 0);
      })
      .catch((err) => {
        console.log(err);
        alert(err);
      });
  };
  useEffect(() => {
    getDataId(id);
  }, []);
  const handleImage = (value) => {
    setImage(value);
  };
  let price = data.price;
  let discountPercent = data.discountPercentage;
  let diskon = price - discountPercent;
  let total = price - diskon;
  console.log(
    "total diskon",
    `Rp. ${total
      .toFixed(0)
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ".")},000`
  );
  return (
    <div className="row pt-3 ms-4 overflow-hidden">
      <div className="col-sm-4">
        <div className="card">
          <img src={image} className="card-img-top" alt="Test" />
        </div>
        <div className="row mt-3">
          <div className="col-sm-4">
            <div className="card h-100">
              <img
                src={data.images[0]}
                className="card-img-top img-fluid"
                onClick={() => handleImage(data.images[0])}
                height="100px"
                alt="Thumbnail1"
              />
            </div>
          </div>
          <div className="col-sm-4">
            <div className="card h-100">
              <img
                src={data.images[1]}
                className="card-img-top img-fluid"
                alt="Thumbnail2"
                height="100px"
                onClick={() => handleImage(data.images[1])}
              />
            </div>
          </div>
          <div className="col-sm-4">
            <div className="card h-100">
              <img
                src={data.images[2]}
                onClick={() => handleImage(data.images[2])}
                className="card-img-top img-fluid"
                height="100px"
                alt="Thumbnail3"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="col-sm-4 fw-bold fs-5">
        <p>Nama Product : {data.title}</p>
        <p>Deskripsi : {data.description}</p>
        <p>
          Harga Normal:{""}
          <s className="fw-bold text-danger">{` Rp. ${data.price
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")},000`}</s>
        </p>
        <p>
          Harga Promo :{" "}
          {`Rp. ${total
            .toFixed(0)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")},000`}
        </p>
        <p>Rating : {data.rating}</p>
        <p>Stock : {data.stock}</p>
        <p>Brand : {data.brand}</p>
        <p>Kategori : {data.category}</p>
      </div>
    </div>
  );
};

export default DetailProduct;
