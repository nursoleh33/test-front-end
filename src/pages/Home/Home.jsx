import React, { useEffect, useState } from "react";
import { getAllProduct, getProduct, getProductDetail } from "../../utils";

const Home = () => {
  const [data, setData] = useState([]);
  // GetData
  const getData = async () => {
    await getAllProduct()
      .then((res) => {
        console.log("res", res);
        setData(res.products);
      })
      .catch((err) => {
        console.log(err);
        alert(err.code);
      });
  };
  useEffect(() => {
    getData();
  }, []);
  console.log("data response", data);
  const limitProducts = data.filter((item) => {
    return parseInt(item.id) <= 10;
  });
  // {
  //   data.map((item) => {
  //     return <>{console.log(item)}</>;
  //   });
  // }
  return (
    <div className="mt-5 pt-3 text-black text-capitalize container-fluid">
      <table class="table table-responsive-sm table-striped text-center">
        <thead className="table-secondary">
          <tr>
            <th scope="col">No.</th>
            <th scope="col">Nama Product</th>
            <th scope="col">Deskripsi</th>
            <th scope="col">Harga Normal</th>
            {/* <th scope="col">Harga Promo</th> */}
            <th scope="col">Stock</th>
            <th scope="col">Photo</th>
          </tr>
        </thead>
        {/* Map */}

        <tbody>
          {limitProducts.map((item) => (
            <tr key={item.id}>
              <th scope="row">{item.id}</th>
              <td>{item.title}</td>
              <td>{item.description}</td>
              <td>
                <s className="fw-bold text-danger">{` Rp. ${item.price
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ".")},000`}</s>
              </td>
              {/* <td>{
                
                
                }</td> */}
              <td>{item.stock}</td>
              <td>
                <img
                  src={item.thumbnail}
                  alt="Test"
                  width={200}
                  height={100}
                  className="img-thumbnail"
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Home;
