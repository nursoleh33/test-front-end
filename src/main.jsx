import React from "react";
import ReactDOM from "react-dom/client";
// import App from "./App";
import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import { createBrowserRouter, RouterProvider, Route } from "react-router-dom";
import { Home, Product, DetailProduct } from "./pages";
import { Layout, Error } from "./component";

// DEFAULT BASEURL GLOBAL
import axios from "axios";
axios.defaults.baseURL = import.meta.env.VITE_URL_API; //import from .env

// Create Router
const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    // Handle Error Messege
    errorElement: <Error />,
    children: [
      {
        path: "/",
        element: <Home />,
      },
      {
        path: "/product",
        element: <Product />,
      },
      {
        path: "/detail-product/:id",
        element: <DetailProduct />,
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    {/* <App /> */}
    <RouterProvider router={router} />
  </React.StrictMode>
);
