import React from "react";
import { Outlet } from "react-router-dom";
import { useLocation } from "react-router-dom";
import { Navbar } from "../../parts";

const Layout = () => {
  const { pathname } = useLocation();
  return (
    <>
      <div className="d-flex mb-5 mx-lg-0">
        <div id="page" className="w-100 mx-auto">
          <Navbar />
          <div
            className="bg-info d-flex justify-content-center mt-5"
            style={{ height: "100vh" }}
          >
            <Outlet />
          </div>
        </div>
      </div>
    </>
  );
};

export default Layout;
